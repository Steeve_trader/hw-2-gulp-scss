"use strict";

const burgerMenu = document.querySelector('.header__burger-menu');

document.addEventListener('click', (e) => {
    if (!e.target.closest('.js-menu')) {
      document.querySelector('.header__burger-menu-first-line').classList.remove('active');
      document.querySelector('.header__burger-menu-second-line').classList.remove('active-line');
      document.querySelector('.menu-wrapper').classList.remove('active-wrapper');
    }

    if (e.target.closest('.js-menu-items')) {
        document.querySelector('.header__burger-menu-first-line').classList.add('active');
        document.querySelector('.header__burger-menu-second-line').classList.add('active-line');
        document.querySelector('.menu-wrapper').classList.add('active-wrapper');

        //ця умова не потрібна у випадку якщо б наші посилання дійсно кудись відправляли, але поки що 
        //залишаємо щоб при натисканні на ці елементи у нас не ламалась логіка меню.ґґґ
      }
  });
  
burgerMenu.addEventListener('click', (e) => {
    document.querySelector('.header__burger-menu-first-line').classList.toggle('active');
    document.querySelector('.header__burger-menu-second-line').classList.toggle('active-line');
    document.querySelector('.menu-wrapper').classList.toggle('active-wrapper');
  });

  document.querySelectorAll('a').addEventListener('click', (event) => {
    event.preventDefault();
    //тимчасовий код, щоб перезавантаження сторінки не заважало, при повному функціоналі він не потрібен
  });
